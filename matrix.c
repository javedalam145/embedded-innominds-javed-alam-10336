/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM          96163388563              javedalam145@gmail.com          10336
---------------------------------------------------------------------------------------------
This program is implementation of algorithm based matrix where we are finding the possible 
number of paths to travel from starting to the ending in two-dimensional matrix using C.
*******************************************************************************************/

#include<stdio.h>
#include<stdlib.h>

int main() {
	//declaring the variables 
	int i,j;
	int row,col;
	printf("enter the rows and columns\n");
	//taking the rows and coloumns from the user
	scanf("%d%d",&row,&col);
	//array declaration
	int array[row][col];
	//operation on arrays to get the possible number of paths.
	for(i=0;i<row;i++) {
		for(j=0;j<col;j++) {
			if(i==0 || j==0) {
				array[i][j] = 1;
			} else {
				array[i][j] = array [i-1][j]+array[i][j-1];
			}
			if((i == row-1) && (j == col-1)) {
				printf("possible number of paths is %d\n", array[i][j]);

			}
		}
	}
	return 0;
}
/*************************output of the program*************************************************
enter the rows and columns
7
6
possible number of paths is 462
javed@javed-Aspire-4738:~/trainingVizag/data_struct$ ./a.out
enter the rows and columns
8
9
possible number of paths is 6435
***********************************************************************************************/
