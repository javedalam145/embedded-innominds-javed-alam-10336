/*******************************************************************************************
 *   NAME	CONTACT NO.	 EMAIL-ID			EMPLOYEE ID
 *   ------------------------------------------------------------------------
 *   JAVED ALAM	96163388563     javedalam145@gmail.com		10336
 *Purpoe-
 This program i the implementation of binarysearch using recursive method in c.
 ****************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
//function definition to sort the array before performing binary search
int *SortingArray(int array[],int size) {
	int i,j,temp;
	for(i=0;i<size;i++) {
		for(j=0;j<size-1;j++) {
			if(array[j] > array[j+1]) {
				temp = array[j];
				array[j] = array[j+1];
				array[j+1] = temp;
			}
		}
	}
	return array;
}
//function definition to search the element using binarysearch method
int BinarySearch(int arry[],int start,int end,int data) {
	int mid;
	if(end < start) {
		return -1;
	}
	mid = (start+end)/2;
	if(arry[mid] == data){
		return mid;
	}
	else if(arry[mid] > data) {
		BinarySearch(arry,start,mid-1,data);
	}
	else if(arry[mid] < data) {
		BinarySearch(arry,mid+1,end,data);
	}
}

//main function to test the above functionality
int main() {
	//variable declaration and initialisation
	int arr[50],i,size,data;
	printf("enter the array size\n");
	scanf("%d",&size);
	printf("enter the array elements\n");
	for(i=0;i<size;i++) {
		//	arr[i] = rand()%101;
		scanf("%d",&arr[i]);
	}
	//array has to be in sorted order to implement binary search
	SortingArray(arr,size);
	printf("****After sorting****\n");
	for(i=0;i<size;i++) {
		printf("%d\t",arr[i]);
	}
	printf("********\n");
	printf("\nenter the data to find\n");
	scanf("%d",&data);
	int result = BinarySearch(arr,0,size-1,data);
	if(result==-1)
		printf("data not found\n");
	else
		printf("element found at index=%d\n",result);
	return 0;
}
/*********************output of the program********************
enter the array size
10
enter the array elements
9
2
8
3
1
4
7
5
6
28
****After sorting****
1	2	3	4	5	6	7	8	9	28	********

enter the data to find
9
element found at index=8
***************************************************************************/
