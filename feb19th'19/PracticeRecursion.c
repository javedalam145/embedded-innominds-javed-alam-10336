/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM          96163388563              javedalam145@gmail.com          10336
---------------------------------------------------------------------------------------------
Purpose-
---------
This program is the implementation of the following operations done using recursion in c-
	1.factial of a number
	2.fibonacci number
	3.conversion of decimal to binary
***********************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
//function definition to calculate factorial of a number
int Fact(int num) {
	if(num == 0)
		return 1;
	return num*Fact(num-1);
}
//function definition to calculate the fibonacci number
int Fibonacci(int num) {
	if(num <= 1)
		return num;
	return Fibonacci(num-1)+Fibonacci(num-2);
}
//function definition to convert the decimal number to binary
int DeciToBinary(int num) {
	if(num == 0)
		return 0;
	return (num%2+10*(DeciToBinary(num/2)));
}
//main function to test the above functionality
int main() {
	int num,choice;
	while(1) {
		printf("enter the operation you want to perform using recursion\n");
		printf("1.Find Factorial\n2.Find Fibonacci Num at the position\n3.Covert Decimat to Binary\n4.Exit\n");
		scanf("%d",&choice);
		switch(choice) {
			case 1:printf("please enter the num\n");
			       scanf("%d",&num);
			       printf("the factorial value is:%d\n",Fact(num));
			       break;
			case 2:printf("please enter the num\n");
			       scanf("%d",&num);
			       printf("the fibonacci value is:%d\n",Fibonacci(num));
			       break;
			case 3:printf("please enter the num\n");
			       scanf("%d",&num);
			       printf("the binary value is:%d\n",DeciToBinary(num));
			       break;
			case 4:exit(0);
			default :
			       printf("please enter the valid optionns\n");       
		}	
	}
}
/*****************************output of the program********************************************
enter the operation you want to perform using recursion
1.Find Factorial
2.Find Fibonacci Num at the position
3.Covert Decimat to Binary
4.Exit
1
please enter the num
4
the factorial value is:24
enter the operation you want to perform using recursion
1.Find Factorial
2.Find Fibonacci Num at the position
3.Covert Decimat to Binary
4.Exit
2
please enter the num
3
the fibonacci value is:2
enter the operation you want to perform using recursion
1.Find Factorial
2.Find Fibonacci Num at the position
3.Covert Decimat to Binary
4.Exit
3
please enter the num
10
the binary value is:1010
enter the operation you want to perform using recursion
1.Find Factorial
2.Find Fibonacci Num at the position
3.Covert Decimat to Binary
4.Exit
4
*************************************************************************************************/
