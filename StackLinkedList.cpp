/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM              96163388563             javedalam145@gmail.com          10336
---------------------------------------------------------------------------------------------
This program is implementation of stack in data structure using LinkedList.
*******************************************************************************************/
#include<iostream>
#include<stdlib.h>

using namespace std;

//creating a structure of type node
struct node{
	//declaring the variables
	int data;
	struct node *next;
};
int length=0;

//creating a class of Structure type
class MyStack{

	struct node *top;
	public:

	//default constructor
	MyStack(){
		top = NULL;
	}
	//member functions
	void push(int);
	int pop();
	void displayData();
};

//method to add the data into the stack
void MyStack :: push(int data){
	node *newnode = new node;
	newnode->data = data;
	newnode->next = NULL;
	length++;
	if(top == NULL)
	{
		top = newnode;
		return;
	}
	node *temp;
	for(temp = top;temp->next!=NULL;temp = temp->next);
	temp->next = newnode;
	return;
}

//method to display the data from the stack
void MyStack :: displayData()
{
	if(top==NULL)
	{
		cout<<"Stack underflow"<<endl;
	}else{
		node *temp;
		cout<<"\t*****data present in stack:*****"<<endl;
		for(temp = top;temp!=NULL;temp=temp->next){
			cout<<"\t\t\t"<<temp->data<<endl;
			//cout<<length;

		}
	}

}

//method to pop the data from the stack
int MyStack :: pop()
{
	node *temp,*prev;
	int data;
	if(top==NULL)	
	{
		cout<<"stack is empty"<<endl;
		return 0;
	}
	if(top->next==NULL){
		data = top->data;
		top = NULL;
		length--;
		return data;
	}
	for(temp=top;temp->next!=NULL;prev = temp, temp = temp->next);
	prev->next = NULL;
	data = temp->data;
	delete temp;
	temp = NULL;
	length--;
	return data;
}

int main()
{
	//declaring the variables
	int choice;
	int data;
	MyStack stack;
	while(1){
		cout<<"************MENU**************"<<endl;
		cout<<"1.push data into the stack"<<endl<<"2.pop data from the stack"<<endl<<"3.displaying the data"<<endl<<"4.EXIT"<<endl;
		cin>>choice;
		switch(choice){
			case 1:cout<<"enter the data"<<endl;
			       cin>>data;
			       stack.push(data);
			       break;
			case 2:data=stack.pop();
			       if(data)
				       cout<<"data:"<<data<<" was popped from the stack"<<endl;
			       break;
			case 3:stack.displayData();
			       break;
			case 4:exit(0);
			default:cout<<"please give proper input";
		}
	}
	return 0;	
}

/************************************output of the program**********************************************
 ************MENU**************
 1.push data into the stack
 2.pop data from the stack
 3.displaying the data
 4.EXIT
 1
 enter the data
 1
 ************MENU**************
 1.push data into the stack
 2.pop data from the stack
 3.displaying the data
 4.EXIT
 1
 enter the data
 2
 ************MENU**************
 1.push data into the stack
 2.pop data from the stack
 3.displaying the data
 4.EXIT
 1
 enter the data
 3
 ************MENU**************
 1.push data into the stack
 2.pop data from the stack
 3.displaying the data
 4.EXIT
 1
 enter the data
 4
 ************MENU**************
 1.push data into the stack
 2.pop data from the stack
 3.displaying the data
 4.EXIT
 1
 enter the data
 5
 ************MENU**************
 1.push data into the stack
 2.pop data from the stack
 3.displaying the data
 4.EXIT
 2
data:5 was popped from the stack
 ************MENU**************
 1.push data into the stack
 2.pop data from the stack
 3.displaying the data
 4.EXIT
 3
 *****data present in stack:*****
 1
 2
 3
 4
 ************MENU**************
 1.push data into the stack
 2.pop data from the stack
 3.displaying the data
 4.EXIT
 2
data:4 was popped from the stack
 ************MENU**************
 1.push data into the stack
 2.pop data from the stack
 3.displaying the data
 4.EXIT
3
*****data present in stack:*****
1
2
3
************MENU**************
1.push data into the stack
2.pop data from the stack
3.displaying the data
4.EXIT
4

***************************************************************************************/
