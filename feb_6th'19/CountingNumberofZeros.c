/**************************************************************************************************************************************
  NAME					 EMAIL-ID		    PHONE NUMBER         EMPLOYEE-ID
  ---------------------------------------------------------------------------------------------------------------------------------------	
  Javed Alam				javedalam145@gmail.com	    9616338563           10336
  ---------------------------------------------------------------------------------------------------------------------------------------
  Purpose-
  ------------
  This program is to count the number of trailing zeros after finding the factorial of a given number.
  Here we do not have to calculate the factorial of a number instead we are only concerned with the 
  finding the number of trailing zeroes.
 *************************************************************************************************************/

#include<stdio.h>
#include<math.h>

int main() {
	//variable declaration and initialization
	int data,numberOfZeros = 0,ctr = 1;
	printf("Enter data to find number of trailing zeros\n");
	scanf("%d",&data);
	while(data >= pow(5,ctr)){
		numberOfZeros = numberOfZeros+(data/pow(5,ctr));
		ctr++;
	}
	printf("Number of trailing zeros in the factorial the given data is %d\n",numberOfZeros);
}


/****************************************OUTPUT***********************************************************

  javed@javed-Aspire-4738:~/trainingVizag/feb_6th'19$ gcc CountingNumberofZeros.c -lm -Wall
  javed@javed-Aspire-4738:~/trainingVizag/feb_6th'19$ ./a.out 
  Enter data to find number of trailing zeros
  60
  Number of trailing zeros in the factorial the given data is 14
  javed@javed-Aspire-4738:~/trainingVizag/feb_6th'19$ ./a.out 
  Enter data to find number of trailing zeros
  100
  Number of trailing zeros in the factorial the given data is 24
 *********************************************************************************************************/
