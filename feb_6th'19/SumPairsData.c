/**************************************************************************************************************************************
 	NAME					 EMAIL-ID		 PHONE NUMBER         EMPLOYEE-ID
---------------------------------------------------------------------------------------------------------------------------------------	
    Javed Alam					javedalam145@gmail.com	 9616338563           10336
---------------------------------------------------------------------------------------------------------------------------------------

This program is a implementaion of finding the sum of the two values equals to the given value in the array. 
If the value is found it returns indices of the values else it will returns NULL.
***************************************************************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#define SIZE 6

//function to find the sum of values equal to the given data
int* getArray(int* arr,int* arr1,int data){
	int i,j;
	arr1=(int*)malloc(2*(sizeof(int)));
	for(i=0,j=SIZE-1;i<j;){
		if((arr[i]+arr[j])>data){
			j--;
		}else if((arr[i]+arr[j])<data){
			i++;
		}else{
			arr1[0]=i;
			arr1[1]=j;
			return arr1;
		}
	}
	return NULL;
}

int main(){
	int arr[SIZE]={1,6,12,24,25,36};
	int *arr1 = NULL,data;
	printf("Enter the value to find \n");
	scanf("%d",&data);
	arr1 = getArray(arr,arr1,data);
	if(arr1==NULL){
		printf("Sum of the values for %d is not found\n",data);
	}else{
		printf("values present at the indexes i=%d j=%d is equal to the given data %d\n",arr1[0],arr1[1],data);
	}
	return 0;
}
/************************************************OUTPUT OF THE PROGRAM**********************************************************
javed@javed-Aspire-4738:~/trainingVizag$ ./a.out 
Enter data 
21
Sum of the values for 21 is not found
javed@javed-Aspire-4738:~/trainingVizag$ ./a.out 
Enter data 
7
values present in the indexes i=0 j=1 is equal to the given data 7

**************************************************************************************************************************************/ 
