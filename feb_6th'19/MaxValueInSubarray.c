/**************************************************************************************************************************************
  NAME					 EMAIL-ID		    PHONE NUMBER         EMPLOYEE-ID
  ---------------------------------------------------------------------------------------------------------------------------------------	
  Javed Alam				javedalam145@gmail.com	    9616338563           10336
  ---------------------------------------------------------------------------------------------------------------------------------------
  Purpose-
----------------
 In this program we find the maximum sum possible with a sub array that can be created using the given array.
******************************************************************************************************************************/

#include<stdio.h>
#define SIZE 10

//functions prototype
int maxSubArraySum(int a[],int size);
int max(int a,int b);
//main function to check the functionality implemented
int main(){
	//decalring the variables
	int i,result;
	//declaring and initializing the array
	int arr[SIZE] = {1,2,-3,4,5,7,-4,3,-2,-3};
	for(i=0;i<SIZE;i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");
	result=maxSubArraySum(arr,SIZE);
	printf("maximum value=%d\n",result);
}

//function to find the subarray which has maximum value
int maxSubArraySum(int a[],int size){
	int i;
	int max_so_far = a[0];
	int curr_max = a[0];
	for(i=1;i<size;i++){
		curr_max = max(a[i],curr_max+a[i]);
		max_so_far = max(max_so_far,curr_max);

	}
	return max_so_far;
}

//function to finding the maximum value
int max(int a,int b){
	if(a>b){
		return a;
	}
	else{
		return b;
	}
}

/*******************************************OUTPUT FOR THE ABOVE PROGRAM**************************************************************
1  2  -3  4  5  7  -4  3  -2  -3  
maximum value=16
**************************************************************************************************************************************/
