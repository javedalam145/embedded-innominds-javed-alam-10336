/**************************************************************************************************
  NAME                  PHONE NUMBER           EMAIL ID
JAVED ALAM              96163388563            javedalam145@gmail.com          10336

***************************************************************************************************
This program is implementation of IPC mechanism which include concept of shared memeory,thread
and semaphore. Here we are taking a common shared memory, which is being accessed by client and 
server to get the message.
**************************************************************************************************/
  
#include <iostream> 
#include <sys/ipc.h> 
#include <sys/shm.h>
#include <unistd.h>  
#include <stdio.h> 
using namespace std; 
//creating a server class
class Server {
	//declaring the variables
	int shmid ;

	public :
	//creating the default  constructor
	Server() {

		key_t key = ftok("myfile1",85);
		shmid= shmget(key,1024,0666|IPC_CREAT); 


		char *str= (char *)shmat(shmid,(void*)0,0);  

		fgets(str,1024,stdin);
		shmdt(str);
	}
	//method to print the name of the client  
	void printMessage() {

		char *str1 = (char *)shmat(shmid,(void*)0,0);  
		cout<<"HELLO............."<< str1<<endl;
	}
};

int main() 
{ 
	int i=0;
	Server s;
	while(i<3) {
		i++;
		sleep(20);
		s.printMessage();
	}
	return 0; 
}

/**************Output of the program*****************************************
imvizag@administrator-ThinkCentre-M82:~/linux$ ./a.out 
hello
HELLO.............vidya
HELLO.............javed
HELLO.............hema
****************************************************************************/ 
