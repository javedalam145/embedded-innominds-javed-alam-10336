#include"header.h"		//declaring user defined header file


int main() {
	// variable declaration and initialisation
	int num1=0,num2=0,*result;
	result = (int *)malloc(sizeof(int));
	printf("enter num 1 and num 2 : \n");
	scanf("%d%d",&num1,&num2);
	while(1){
		// operation menu to perform various mathematical operations
		int choice;
		printf("************************ CALCULATOR *****************\n");
		printf("1) ADDITION\n2) SUBTRACTION\n 3) MULTIPLICATION\n4)DIVISION\n5) EXIT\n");
		printf("enter u r choice\n");
		scanf("%d",&choice);
		switch(choice){
			case 1: addition(num1,num2,result);
				printf("the result after addition is %d\n",*result);
				break;

			case 2: subtraction(num1,num2,result);
				printf("the result after subtraction is %d\n",*result);
				break;
			
			case 3: multiplication(num1,num2,result);
			        printf("the resullt after multiplication is %d\n",*result);
			        break;

			case 4: division(num1,num2,result);
			        printf("the result after division is %d\n",*result);
			        break;
			
			case 5: exit(0);	
			default: printf("enter valid option\n");
		}
	}
	return 0;
}
