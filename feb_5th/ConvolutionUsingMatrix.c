/************************************************************************************************************
    NAME 		MOBILE NUMBER 			EMAIL-ID 			EMPLOYEE-ID
------------------------------------------------------------------------------------------------------------
   JAVED ALAM	 	9616338563			javedalam@gmail.com		10336 -----------------------------------------------------------------------------------------------------------
This program is the implementation of convolution theorem. In this program we are taking two matrices
and performing the convolution method on them.
*************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#define SIZE 3

//function definition for printing the matrix
void displayMatrix(int rows,int columns,int arr[rows][columns]) {
	int i,j;	
	for(i=0;i<rows;i++){
		for(j=0;j<columns;j++){
			printf("%d\t",arr[i][j]);
		}
		printf("\n");
	}
}
//main function to check the above functionality
int main() {
	//variable declaration and initialisation
	int row,column,i,j,sum=0,k,l;
	//generating random matrix
	srand(getpid());
	printf("Enter row and column values\n");
	scanf("%d %d",&row,&column);
	//declaring the main matrix
	int arr[row][column];
	//declaring the resultant matrix 
	int resultant[row][column];
	//declaring the window matrix
	int window[SIZE][SIZE];
	for(i=0;i<row;i++){
		for(j=0;j<column;j++){
			//generating the main matrix elements randomly
			arr[i][j]=rand()%10;
			resultant[i][j] = 0;
		}
	}
	printf("Displaying the main matrix\n");
	displayMatrix(row,column,arr);
	printf("Displaying the resultant matrix before convolution\n");
	displayMatrix(row,column,resultant);
	for(i=0;i<SIZE;i++){
		for(j=0;j<SIZE;j++){
			//generating the window matrix elements randomly
			window[i][j]=rand()%10;
		}
	}
	//dispalying the window matrix
	printf("Displaying the window matrix\n");
	displayMatrix(SIZE,SIZE,window);
	//logic for convolution
	for(i=0;i<row-2;i++){
		for(j=0;j<column-2;j++){
			sum = 0;
			for(k=0;k<=2;k++){
				for(l=0;l<=2;l++){
					sum+=arr[i+k][j+l]*window[k][l];
				}
			}
			resultant[i+1][j+1]=sum;
		}
		
	}
	printf("Displaying the resultant matrix after convolution\n");
	displayMatrix(row,column,resultant);
	return 0;
}
/************************************OUTPUT OF THE ABOVE PROGRAM**************************************
Enter row and column values
7
8
Displaying the main matrix
6	7	6	0	0	4	3	3	
7	6	1	6	8	9	6	5	
5	5	2	9	2	7	3	2	
7	2	3	3	3	2	8	1	
1	5	2	2	1	7	7	0	
4	9	6	2	8	4	9	4	
0	1	5	2	0	0	6	7	
Displaying the resultant matrix before convolution
0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	
Displaying the window matrix
4	9	2	
7	3	1	
9	5	8	
Displaying the resultant matrix after convolution
0	0	0	0	0	0	0	0	
0	253	260	136	230	208	229	0	
0	233	152	183	248	264	226	0	
0	177	153	162	179	212	196	0	
0	205	211	194	157	245	265	0	
0	159	175	139	91	206	236	0	
0	0	0	0	0	0	0	0	
*********************************************************************************************************/ 

