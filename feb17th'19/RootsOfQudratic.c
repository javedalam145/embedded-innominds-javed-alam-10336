/******************************************************************************************
  NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
  ____________________________________________________________________________________________
  JAVED ALAM           96163388563             javedalam145@gmail.com          10336
  -------------------------------------------------------------------------------------------- 
  Purpose:-
----------------

	-the program deals with the finding of roots for a given two degree qudratic coefficients 
------------------------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<math.h>
int main() {
	int a,b,c;
	float root1,root2;
	printf("enter the coefficients of qudratic equation \n");
	scanf("%d %d %d",&a,&b,&c);
	while(a==0) {
		printf("highest coefficient cannot be zero....please renter \n");
		scanf("%d",&a);
	}
	float r=(b*b)-4*a*c;
	if(r<0) {
		r=-r;
		root1=(float)-b/(2*a);
		printf("%f\n",root1);
		root2=sqrt(r)/(2*a);
		printf("the 1st complex  root of equation is %f+i%f\n",root1,root2);
		printf("the 2nd complex root of equation is %f-i%f\n",root1,root2);
	} else {


		root1=(-b+sqrt(r))/(2*a);
		root2=(-b-sqrt(r))/(2*a);
		printf("the 1st root of equation is %f\n",root1);
		printf("the 2nd root of equation is %f\n",root2);
	}
	return 0;
}

/*********************OUTPUT*************************************8
 
enter the coefficients of qudratic equation 
12
10
16
-0.416667
the 1st complex  root of equation is -0.416667+i1.076904
the 2nd complex root of equation is -0.416667-i1.076904

**********************************************************************/

