/*NOTE- This program has been done in pair-programming */
/**********************************************************************************************
GROUP-A
NAME                  PHONE NUMBER           EMAIL ID				EMPLOYEE ID
--------------------------------------------------------------------------------------------
JAVED ALAM              96163388563             javedalam145@gmail.com          10336

This program is an object oriented design for a small Grocery shop, which allows the customer
to buy and pay the number of items available in the Grocery shop. User can continuing buying
items. Once customer is done, the payment can be done thru PoS(Point of Sale) 
 **********************************************************************************************/ 

#include<cstring>
#include<stdlib.h>
#include<unistd.h>
#include<iostream>
#include<map>
using namespace std;

//creating a grossary class
class Grocery
{	
	static int total;
	map<int,pair<string,int>> grocery;
	public:
	//creating a map	
	Grocery(map<int,pair<string,int>> grocery){

		this->grocery=grocery;

	}
	//displaying the items in the  grocery
	void displayItems() {
		cout<<"itemcode"<<"  "<<"itemname"<<"  "<<"item price"<<endl;
		for(auto i:grocery)
		{
			cout<<i.first;
			cout<< " " <<i.second.first<<" "<<i.second.second<<endl;
		}
	}
	//method to search items by its key
	string searchByKey(int codeNum) {

		for(auto i:grocery)
		{

			if(i.first==codeNum);
			return i.second.first;
		}
		return NULL;
	}
	//method to search item by its name
	int searchByItemName(string itemName) {

		for(auto i:grocery)
		{
			if(i.second.first==itemName);
			return i.second.second;
		}
		return 0;
	}

	//function for buying the items 
	void doBuying()
	{
		//declaring the variables

		int itemCode;
		int itemQuantity;
		int amount;
		char choice;
		cout<<"enter the item code and quantity"<<endl;
		cin>>itemCode>>itemQuantity;
		string itemName=searchByKey(itemCode);
		generateBill(itemName,itemQuantity);
		if(total!=0)
		{
			while(1){
				cout<<"1.Generate bill"<<endl<<"2.continue_shopping"<<endl;
				cout<<"enter your choice";
				cin>>choice;
				switch(choice)
				{
					case '1':doPayment();
						 exit(0);
						 break;
					case '2':displayItems();
						 doBuying();
						 break;	
					default:cout<<"invalid option"<<endl;
				}	
			}
		}
	}
	//display method 
	void thankYouNote()
	{
		cout<<"successful payment"<<endl;
		cout<<"******THANKYOU********"<<endl;
		cout<<"******VISIT AGAIN********"<<endl;
	}



	//function for bill generation
	void generateBill(string itemName,int itemQuantity)
	{
		int flag=0;


		//condition to compare entered item with the items available in the grossary
		if(searchByItemName(itemName))
		{
			total+=itemQuantity*searchByItemName(itemName);
			flag=1;
			//break;
		}


		if(flag==0)
		{
			cout<<"item not found"<<endl;
		}
	}

	//payment function to pay the bills
	void doPayment(){
		int amount;
		int flag=0;
		cout<<"total bill="<<total<<endl;	
		while(flag==0){
			cout<<"enter the amount"<<endl;   
			cin>>amount;
			//condition to check entered amount is less than total cost
			if(amount<total){
				total-=amount;
				cout<<total<<" more need to pay"<<endl;
			}else if (amount==total){ //condition to check amount is equal to total cost
				flag=1;
				thankYouNote();
			}else if(amount>total){ //condition to check amount is more than total cost
				total=amount-total;
				flag=1;	
				cout<<"take your remaining balance "<<total<<endl;
				thankYouNote();
			}
		}
	}
};

//decalring the static variable	
int Grocery::total;
int main()
{	

	map<int,pair<string,int>> grocery;

	grocery.insert(make_pair(1,make_pair("soap",20)));
	grocery.insert(make_pair(2,make_pair("Paste",50)));
	grocery.insert(make_pair(3,make_pair("Chocolate",15)));
	grocery.insert(make_pair(4,make_pair("Milk",25)));
	grocery.insert(make_pair(5,make_pair("orange",15)));
	grocery.insert(make_pair(6,make_pair("apple",15)));
	grocery.insert(make_pair(7,make_pair("grapes",20)));
	grocery.insert(make_pair(8,make_pair("watermelon",50)));
	grocery.insert(make_pair(9,make_pair("potato",25)));
	grocery.insert(make_pair(10,make_pair("ladiesfinger",40)));
	grocery.insert(make_pair(11,make_pair("bitterguard",25)));
	grocery.insert(make_pair(12,make_pair("tomato",20)));
	Grocery gro(grocery);
	//declaring variables
	char choice;

	while(1){
		//menu to display the items
		cout<<endl<<"****MENU*****"<<endl;
		cout<<"1.Display_the_items"<<endl<<"2.Buying_the_items"<<endl<<"3.Exit"<<endl;
		cout<<"enter your choice";			
		cin>>choice;
		switch(choice)
		{
			case '1':gro.displayItems();
				 break;
			case '2':gro.displayItems();
				 gro.doBuying();
				 break;
			case '3':exit(0);
			default:cout<<"please enter valid option"<<endl;
		}
	}
	return 0;
}
/********************************************output of the program**************************************
****MENU*****
1.Display_the_items
2.Buying_the_items
3.Exit
enter your choice1
itemcode  itemname  item price
1 soap 20
2 Paste 50
3 Chocolate 15
4 Milk 25
5 orange 15
6 apple 15
7 grapes 20
8 watermelon 50
9 potato 25
10 ladiesfinger 40
11 bitterguard 25
12 tomato 20

****MENU*****
1.Display_the_items
2.Buying_the_items
3.Exit
enter your choice2
itemcode  itemname  item price
1 soap 20
2 Paste 50
3 Chocolate 15
4 Milk 25
5 orange 15
6 apple 15
7 grapes 20
8 watermelon 50
9 potato 25
10 ladiesfinger 40
11 bitterguard 25
12 tomato 20
enter the item code and quantity
3 4
1.Generate bill
2.continue_shopping
enter your choice1
total bill=80
enter the amount
100
take your remaining balance 20
successful payment
******THANKYOU********
******VISIT AGAIN********
*/
