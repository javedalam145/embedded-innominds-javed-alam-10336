#include"Client.h"

//declaring the global variables
int n;
int sockfd,portNum;
char buf[1024];
char buff[4]={'b','y','e','\n'};

//function to print error message 
void error(char *msg){
	perror(msg);
	exit(1);
}

//function to print the data sent by the client
void * clientRead(void* arg) {
	while(1)
	{
		memset(buf,0,sizeof(buf));
		n = read(sockfd,buf,1024);
		if(n<0)
			error("ERROR ON READING");
		if(!(strcmp(buf,buff))) {

			printf("server=%s\n",buf);
			exit(1);
		}
		else
			printf("server=%s\n",buf);
	}
	return NULL;
}

//function to write the data by the client
void * clientWrite(void* arg) {
	while(1){
		memset(buf,0,sizeof(buf));
		fgets(buf,1024,stdin);
		n = write(sockfd,buf,strlen(buf));
		if(n<0)
			error("ERROR ON WRITING");
		if(!(strcmp(buf,buff))) 
			exit(1);
	}
	return NULL;
}

//in main method we create socket, connect and create threads
int main(int argc, char **argv){
	//declaring the variables
	struct hostent *server;
	pthread_t thread_id,thread_id1;
	struct sockaddr_in serv_addr;
	if(argc<3){
		fprintf(stderr,"usage %s hostname port\n",argv[0]);
		exit(0);
	}
	//converting the portnum from ascii to integer
	portNum = atoi(argv[2]);
	//creating socket connections
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	//getting the hostname
	server = gethostbyname(argv[1]);
	if(server == NULL){
		fprintf(stderr,"error,no such host\n");
		exit(0);
	}
	//formatting the buffer with zero
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	//socket structure declaration
	serv_addr.sin_family = AF_INET;
	strncpy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port = htons(portNum);
	//connecting the client to socket
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
		error("ERROR IN CONNECTING");
	//creating the threads for communication
	pthread_create(&thread_id,NULL,clientRead , NULL);
	pthread_create(&thread_id1,NULL,clientWrite, NULL);
	//joining the thread
	pthread_join(thread_id, NULL);
	pthread_join(thread_id1, NULL);
	//closing the thraeds
	close(sockfd);
	return 0;
}
