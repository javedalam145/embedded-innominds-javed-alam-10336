#include"Server.h"

//declaring the global variables
int sockfd,newSockfd,portNum;
char buf[1024];
int n;
char buff[4]={'b','y','e','\n'};

//function to print the error message
void error(char *msg){
	perror(msg);
	exit(1);
}


// main method we create socket, bind ip-address and port number, in accept we server accepts the client request using pthreads we write abd read the data
int main(int argc, char **argv){
	//declaring the variables
	struct sockaddr_in serv_addr, cli_addr;
	pthread_t thread_id,thread_id1;
	socklen_t cliLen;
	if(argc<2){
		fprintf(stderr,"ERROR, no port available\n");
		exit(1);
	}
	//creating socket to connect to client
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	//clearing off the memory
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	//converting the portnum from ascii to integer
	portNum = atoi(argv[1]);
	//server structure decalaration
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNum);
	//binding the socket on server
	if(bind(sockfd,(struct sockaddr*) &serv_addr, sizeof(serv_addr))<0)
		error("ERROR ON BINDING");
		//listrening from the client
	listen(sockfd,5);
	cliLen = sizeof(cli_addr);
	//blocking call from server side it will wait for the client connection
	newSockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cliLen);
	
	if(newSockfd<0)
		error("ERROR IN ACCEPTING");
	close(newSockfd);
	close(sockfd);
	return 0;
}
