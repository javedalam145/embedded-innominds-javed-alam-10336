#include<stdio.h>
#include<stdlib.h>

//function definition to search the element using binary search method
int binarySearch(int low,int high,int array[],int data) {
	int mid;		//declaration of variables
	if(low>high)
		return -1;
	mid = (low+high)/2;
	//checking if the data is present at middle index position
	if(array[mid] == data)
		printf("data index is %d",mid);		//printing the index position
		return data;		//if present then returninig the data
	//if data is less than the values of the array on right side then checking on left side	
	if(array[mid]>data){
		mid = mid-1;
		//calling the search method recursively
		binarySearch(low,mid,array,data);
	} else {
		//else if data is less than the values of the array on left side then checking on right side	
		mid = mid+1;
		//calling the search method recursively
		binarySearch(mid,high,array,data);
	}
	//return 0;
}
//main method to test the above functionality
int main() {
	int size,index,data;			//variable declaration
	//array declaration and initialisation
	int arr[] = {3,5,7,9,10,14,16,35,67,89,90};
	//getting the size of the array
	size = sizeof(arr)/sizeof(arr[0]);
	//printf("%d",size);
	while(1) {
	printf("please enter a number to search\n");
	scanf("%d",&data);		//taking the data from user to search
	index = binarySearch(0,size-1,arr,data);		//calling the binarySearch method
	if(index == 0)
		printf("data not found\n");
	else
		printf("found data is:%d\n",index);
	return 0;
	}
}
