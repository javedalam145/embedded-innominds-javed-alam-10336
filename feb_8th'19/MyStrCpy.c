/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM           96163388563             javedalam145@gmail.com          10336
-------------------------------------------------------------------------------------------- 
Purpose
-----------
This is the implementation of strcpy() in string, which is used for copying of two strings.
*************************************************************************************************/

#include<stdio.h>
//function prototype
void strcpy(char*,char*);

int main() {
	//variable declaration and initialisation
	char source[100],destination[100];
	printf("enter the source string: \n");
	gets(source);
	strcpy(source,destination);
	printf("the copied string at destnation is %s\n",destination);
	return 0;
}
//function definition to copy the source string to destination string
void strcpy(char source[],char destination[]) {
	int i = 0;
	while(source[i]!= '\0')
	{
		destination[i] = source[i];
		i++;
	}
	 destination[i] = '\0';
}
/*******************************output of the program***************************************
enter the source string: 
javed alam
the copied string at destnation is javed alam
********************************************************************************************/
