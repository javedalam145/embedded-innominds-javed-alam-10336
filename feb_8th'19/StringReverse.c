/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM           96163388563             javedalam145@gmail.com          10336
-------------------------------------------------------------------------------------------- 
Purpose
-----------
this is the implementation of strrev() in string, which is used for reversing the each words of the string.
*************************************************************************************************/
#include<stdio.h>

int main() {
	//string declaration
	char arr[100],temp[100];
	int i=0,j=0;
	printf("enter the string:");
	gets(arr);
	//reversing the string
	while(arr[i] !='\0')
		i++;
	while(i>0) {
		temp[j] = arr[--i];
		j++;
	}
	temp[j] = '\0';
	//reversing each words after reversing
	printf("string after being reversed\n");
	for(i=0; temp[i] !='\0';i++) {\
		if(temp[i+1] == ' '||temp[i+1] == NULL) {
			for(j=i;j>=0 && temp[j] != ' ';j--)
				printf("%c",temp[j]);
			printf(" ");
		}
	}
	printf("\n");
	return 0;
}
/*********************************output of the program**************************************
enter the string:javed alam
string after being reversed
alam javed 
********************************************************************************************/

