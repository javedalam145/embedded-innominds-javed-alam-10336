/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM           96163388563             javedalam145@gmail.com          10336
-------------------------------------------------------------------------------------------- 
Purpose
-----------
this is the implementation of string function to remove all the occurrence of duplicate 
character in a string.
*******************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main() {

	//variable initialisation
	char arr[50]="silence is a source of great strength";
	printf("%s\n",arr);
	for(int i=0;arr[i];i++) {
		char temp=arr[i];
		for(int j=i+1;arr[j];j++) {
			if(arr[j]==temp) {
				//memmove function copies the data from source to destination one by one
				memmove(arr+j,arr+j+1,strlen(arr+j+1)+1);
				j--;
			}
		}
	}
	printf("%s\n",arr);
}
/****************************output of the program************************************
javed@javed-Aspire-4738:~/trainingVizag/feb_8th'19$ ./a.out 
silence is a source of great strength
silenc aourfgth
*************************************************************************************/
