/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM           96163388563             javedalam145@gmail.com          10336
-------------------------------------------------------------------------------------------- 
Purpose
-----------
This is the implementation of conversion of string to integer and float.
*************************************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

//converts given string to integer
int sToInteger(char *p) {
	int i,c,d=0,cnt=0;
	for(i=0;i<strlen(p);i++) {
		if(p[i]=='-') {
			cnt++;
			if(cnt>1) {
				break;
			}
		} 
		else if( p[i]>=48 && p[i]<=57) {
			c=p[i]-48;
			d=d*10+c;

		} else {
			break;
		}
	}
	return d;
}

//converts given string to float value 
float sToFloat(char *p) {
	int i,cnt=0,a;
	float b=0,d=0.1;
	a=sToInteger(p);
	printf("%d\n",a);
	for(i=0;i<strlen(p);i++) {
		if(p[i]==46) {
			cnt++;
			i++;
		}
		if(cnt==1) {
			b=b+(p[i]-48)*d;
			d=d*0.1;
		} 
	}
	return (float)a+b;
}
//main function to test the above functionality	
int main() {
	//vriable declaration and initialisation	
	char str[32];
	int i,a,count=0,count1=0,count2=0;
	float b;
	printf("Enter the input to convert to float value or integer value :\n");
	scanf("%s",str);
	getchar();
	for(i=0;i<strlen(str);i++) {
		if(str[i]==46) {
			count++;
		} else if(str[i]>=48 || str[i]<=59 ) {
		}if(str[i]=='-') {
			count2++;
		} else {
			count1++;
		}
	}
	if(count==1) {
		if(str[0]=='-' && count2<=1) {
			b=sToFloat(str);
			printf("the float from string is -%f \n",b);
		} else if(count2>1) {
			printf("not a valid input \n");
		}else {
			b=sToFloat(str);
			printf("the float from string is %f \n",b);
		}
	} else {
		if(str[0]=='-' && count2<=1) {
			a=sToInteger(str);
			printf("the integer from  string is -%d \n",a);
		} else if(count1==0 || count2>1 || count>1) {
			printf("given input has alphabets or special characters\n");
		} else {
			a=sToInteger(str);
			printf("the integer from  string is %d \n",a);
		}
	}
		return 0;
}
/*******************************OUTPUT*********************************
 Enter the input to convert to float value or integer value :
-1234
the integer from  string is -1234 

$ ./a.out

Enter the input to convert to float value or integer value :
12356

the integer from  string is 12356 

$ ./a.out 
Enter the input to convert to float value or integer value :
-23.465

the float from string is -23.465000 

$ ./a.out 
Enter the input to convert to float value or integer value :
--637

given input has alphabets or special characters

$ ./a.out 
Enter the input to convert to float value or integer value :
-2-23.25

not a valid input 
******************************************************************************/
