/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM           96163388563             javedalam145@gmail.com          10336
-------------------------------------------------------------------------------------------- 
Purpose
-----------
this is the implementation of strcat() in string, which is used for concatination of two strings.
*************************************************************************************************/
#include<stdio.h>
#include<string.h>
//function prototype
void mystrcat(char[],char[]);

int main() {
	//string declaration and initialisation
	char source[100],destination[100];
	printf("enter source string: \n");
	gets(source);
	printf("enter destination string: \n");
	gets(destination);
	mystrcat(source,destination);
	printf("the destination sring after concatination is %s \n",source);
	
}
//function definition two concatinate the source and destination strings.
void mystrcat(char source[],char destination[])
{
	int i = 0,length=0,length1 = 0;
        length = strlen(source);
	while(destination[i]!=0)
	{
		source[length+i] = destination[i];
		i++;
	}
	source[length+i] = '\0';
}
/*******************************output of the program*************************************
javed@javed-Aspire-4738:~/trainingVizag/feb_8th'19$ ./a.out 
enter source string: 
javed
enter destination string: 
alam
the destination sring after concatination is javedalam 
******************************************************************************************/
