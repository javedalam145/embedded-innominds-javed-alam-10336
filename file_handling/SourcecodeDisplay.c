/******************************************************************************************
  NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
  ____________________________________________________________________________________________
  JAVED ALAM           96163388563             javedalam145@gmail.com          10336
  -------------------------------------------------------------------------------------------- 
  Purpose:-
----------------
	This program gives the output along with content present in source file. 
***********************************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	int ch;
	fp = fopen(__FILE__,"r");	
		if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	printf("************\n");
	printf("------------------------------------------------\n");
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
}
/***************************OUTPUT******************************
 
FILE OPENED SUCCESSFULLY
************
------------------------------------------------
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	int ch;
	fp = fopen(__FILE__,"r");	
	//This give the file name which is we were given.
	//printf("%s\n",__FILE__);
	if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	printf("************\n");
	printf("------------------------------------------------\n");
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
}
****************************************************************/
