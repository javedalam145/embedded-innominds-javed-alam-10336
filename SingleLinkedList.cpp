/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM              96163388563             javedalam145@gmail.com          10336

This program is the implementation of single linked list where the user can add the data at
desired position, delete the data and also can view the data. 
*******************************************************************************************/
#include<iostream>
#include<stdlib.h>

using namespace std;
//creating structure of type Node
struct Node
{
	//declaring the variables
	int data;
	Node *next;//self-referential structure
}*head;
int length = 0;
//creating a class of SingleLinkedList type
class SingleLinkedList{
	//declaring the pointer variable
	//struct Node *head;
	public:
	//default constructor
	SingleLinkedList(int data)
	{
		head = new struct Node;
		head->data = data;
		head->next = NULL;
		length++;
	}
	//member functions

	void addNodeAtPosition(int,int);
	void deleteNodeAtPosition(int);
	void displayLinkedListData();
	void updateLinkedList(int);
	void menu();
};
//method to add Node at any position
void SingleLinkedList :: addNodeAtPosition(int position, int data)
{
	if(position < 0 || position > length)
	{
		cout<<"cannot add at that position"<<endl;
	}else if(position == 0){
		Node *newNode = new Node;
		newNode->data = data;
		newNode->next = head;
		head = newNode;
		length++;
		}else if(position == length){
			Node *temp;
			Node *newNode = new Node;
			length++;
			temp = head;
			while(temp->next != NULL)
				temp = temp->next;
			temp->next = newNode;
			newNode->data = data;
			newNode->next = NULL;	
			}else{	
				int index = 1;
				Node *newNode = new Node;
				newNode->data = data;
				Node *currentNode = head;
				Node *prevNode = head;
				while(index != position){
					prevNode = currentNode;
					currentNode = currentNode->next;
					index = index+1;
				}
			prevNode->next = newNode;
			newNode->next = currentNode;
			length++;
			}
		}		
		
	

//displaying the linked list data
void SingleLinkedList :: displayLinkedListData()
{
 if(head == NULL){
	cout<<"List is empty"<<endl;
	}else{
		Node *temp = head;
		while(temp != NULL){
			cout<<temp->data<<endl;
			temp = temp->next;
	}
}
}
void SingleLinkedList :: updateLinkedList(int position)
 {
              if(position<=0 || position > length) {
                        cout<<"cannot update the data at that position:"<<position<<endl;

                }
                int index;
                 Node *temp = head;
                for(index=1;index<position;index++) {
                        temp = temp->next;
                }
                cout<<"enter the data to be update :"<<endl;
                cin>>temp->data;
                return;
              }
//method to delete data from any position
void SingleLinkedList :: deleteNodeAtPosition(int position)
{
	if(position <0 || position >length){
		cout<<"invalid length"<<endl;
	}else if(position == 0){
		Node *temp = head;
		head = head->next; 
		delete temp;
		temp = NULL;
		length = length-1;
		}else{
			int ctr = 0;
			Node *currentNode = head;
			Node *prevNode = head;
			while(ctr != position){
				prevNode = currentNode;
				currentNode = currentNode->next;
				ctr++;
			}
			prevNode->next = currentNode->next;
			delete currentNode;
			currentNode = NULL;
			length--;
		}
}

//method to call other methods and display menu
void SingleLinkedList ::  menu(){
	//declaring the variables
	int choice;
	int data;
	int position;
	while(1){
		cout<<"*************MENU******************"<<endl;
		cout<<"1.add data to the linked list "<<endl<<"2.delete data"<<endl<<"3.display the data"<<endl<<"4.update the data"<<endl<<"5.EXIT"<<endl;
		cout<<"enter your choice";
		cin>>choice;
		switch(choice){
			case 1:cout<<"enter the data";
				cin>>data;
				cout<<"enter position";
				cin>>position;
				addNodeAtPosition(position,data);
				break;
			case 2:cout<<"enter the position where data to be deleted";
				int position;
				cin>>position;
				deleteNodeAtPosition(position);
				break;
			case 3:cout<<"displaying the data elements"<<endl;
				displayLinkedListData();
				break;
			case 4:cout<<"enter the position where data has to be updated";
				cin>>position;
				updateLinkedList(position);
				break;
			case 5:exit(0);
			default:cout<<"enter valid input";
		}
}
}
//driver program to run above program	
int main(){
	SingleLinkedList obj(1);//creating object of SingleLinkedList type
	obj.menu();
	return 0;
}

/********************************output of the program***********************************
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice1
enter the data2
enter position1
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice1
enter the data3
enter position2
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice1
enter the data4
enter position3
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice3
displaying the data elements
1
2
3
4
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice1
enter the data5
enter position4
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice3
displaying the data elements
1
2
3
4
5
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice2
enter the position where data to be deleted1
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice3
displaying the data elements
1
3
4
5
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice4
enter the position where data has to be updated3
enter the data to be update :
7
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice3
displaying the data elements
1
3
7
5
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.update the data
5.EXIT
enter your choice5

*************************************************************************************/
