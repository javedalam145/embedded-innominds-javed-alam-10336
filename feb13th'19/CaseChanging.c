/******************************************************************************************
  NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
  ____________________________________________________________________________________________
  JAVED ALAM           96163388563             javedalam145@gmail.com          10336
  -------------------------------------------------------------------------------------------- 
  Purpose:-
----------------
 This program is the implementation of converting upper case to lower case and vice versa.
**************************************************************************************************************************/
#include<stdio.h>
int main() {
	//variable declaration
	char data;
	printf("Enter  a character\n");
	scanf("%c",&data);
	if((data>=65)&&(data<=91)){
			data = data|' ';
			printf("%c",data);
			}
			else if(((data>=97)&&(data<=122))){
				data = data&'_';
			printf("%c",data);
			}else{
		printf("entered data is integer");
		}
	
	return 0;
}

/***********************OUTPUT*************************************
Enter  a character
t
T
-----------------------------------------------
Enter  a character
K
k
*****************************************************************/
