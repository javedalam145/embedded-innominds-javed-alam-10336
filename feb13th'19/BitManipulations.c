/******************************************************************************************
  NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
  ____________________________________________________________________________________________
  JAVED ALAM           96163388563             javedalam145@gmail.com          10336
  -------------------------------------------------------------------------------------------- 
  Purpose
  -----------
  This program is the implementation of setting, clearing and toggling a bit, checking the status of the bit, checking 
  the lowest set bit, clearing the bits from LSB to given bit position, and counting the set bits and printing its 
  binary equivalent.
 **************************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>

//function definition to display the data in the form of bits
void display(int data){
	int i;
	for(i=31;i>=0;i--){
		printf("%d",(data>>i)&1);
	}
	printf("\n");
}

//function definition to set a bit
void doSetBit(int data,int bit){
	printf("Data before setting a bit\n");
	display(data);
	data=data|(1<<bit);
	printf("Data after setting %d bit\n",bit);
	display(data);
}

//function definition to clear a bit
void doClearBit(int data,int bit){
	printf("Data before clearing a bit\n");
	display(data);
	data=data&(~(1<<bit));
	printf("Data after clearing %d bit\n",bit);
	display(data);
}

//function definition to toggle a bit
void doToggleBit(int data,int bit){
	printf("Data before toggling a bit\n");
	display(data);
	data=data^(1<<bit);
	printf("Data after toggling %d bit\n",bit);
	display(data);
}
//function definition to check the status of a bit on a position
void checkBitStatus(int data, int bit) {
	printf("entered data is : %d\n",data);
	display(data);
	printf("bit at position %d is :%d\n",bit,((data>>bit)&1));
}
//function definition to set lowest bit (LSB)
void lowestSetBit(int data) {
	int i;	
	printf("entered data is :%d\n",data);
	display(data);
	for(i=0;i<=31;i++) {
		if((data>>i)&1) {
			printf("lowest set bit is :%d\n",i);
			return;			
		}
	}
	printf("no set bit in the given number: %d\n",data);
}
//function definition to clear LSB nth bit which will clear the bits from LSB to given bit position.
void clearLsbToNthBit(int data,int bit) {
	int i;	
	printf("entered data before clearing:%d\n",data);
	display(data);
	for(i=0;i<= bit;i++) {
		data = (data&(~(1<<i)));
	}
	printf("the data after clearing upto bit position %d is :%d\n",bit,data);
	display(data);
}
//function definition to count the set bits present in the given data
void doCountSetBits(int data) {
	int setBitCount = 0;
	int i;	
	for(i=0;i<=31;i++) {
		if((data >> i)&1)
			setBitCount++;
	}
	printf("the number of set bits in given data %d = %d\n",data,setBitCount);
}

//driver program to run the above operations
int main() {
	//variable declaration
	int choice,data,bit;
	while(1){
		printf("\n*********MENU FOR BIT MANIPULATIONS*************\n1.set a bit\n2.clear a bit\n3.Toggle a bit\n4.Check bit status at a position\n5.To check set bits\n6.To clear from LSB to nth bit positon\n7.Count the number of set bits\n8.EXIT\n");
		printf("Enter your choice\n");
		scanf("%d",&choice);
		switch(choice){
			case 1:printf("Enter the data\n");
			       scanf("%d",&data);
			       printf("Enter the bit\n");
			       scanf("%d",&bit);
			       doSetBit(data,bit);
			       break;
			case 2:printf("Enter the data\n");
			       scanf("%d",&data);
			       printf("Enter the bit\n");
			       scanf("%d",&bit);
			       doClearBit(data,bit);
			       break;
			case 3:printf("Enter the data\n");
			       scanf("%d",&data);
			       printf("Enter the bit\n");
			       scanf("%d",&bit);
			       doToggleBit(data,bit);
			       break;
			case 4:printf("enter the position of bit which you want to check the status:\n");
			       scanf("%d",&bit);
			       while(bit < 0 || bit >31) {
				       printf("please enter valid bit position\n");
				       scanf("%d",&bit);
			       }
			       checkBitStatus(data,bit);
			       break;
			case 5:lowestSetBit(data);
			       break;
			case 6:printf("please enter the position till where you want to clear the bit\n");
			       scanf("%d",&bit);
			       while(bit < 0 || bit >31) {
				       printf("please enter valid bit position\n");
				       scanf("%d",&bit);
			       }
			       clearLsbToNthBit(data,bit);
			       break;
			case 7:doCountSetBits(data);
			       break;
			case 8:exit(0);

			default:printf("Invalid choice\n");
		}
	}
	return 0;

}

/*********************************************OUTPUT FOR THE ABOVE PROGRAM*****************************************************


 *********MENU FOR BIT MANIPULATIONS*************
 1.set a bit
 2.clear a bit
 3.Toggle a bit
 4.Check bit status at a position
 5.To check set bits
 6.To clear from LSB to nth bit positon
 7.Count the number of set bits
 8.EXIT
 Enter your choice
 1
 Enter the data
 10
 Enter the bit
 1
 Data before setting a bit
 00000000000000000000000000001010
 Data after setting 1 bit
 00000000000000000000000000001010

 *********MENU FOR BIT MANIPULATIONS*************
 1.set a bit
 2.clear a bit
 3.Toggle a bit
 4.Check bit status at a position
 5.To check set bits
 6.To clear from LSB to nth bit positon
 7.Count the number of set bits
 8.EXIT
 Enter your choice
 1
 Enter the data
 10
 Enter the bit
 2
 Data before setting a bit
 00000000000000000000000000001010
 Data after setting 2 bit
 00000000000000000000000000001110

 *********MENU FOR BIT MANIPULATIONS*************
 1.set a bit
 2.clear a bit
 3.Toggle a bit
 4.Check bit status at a position
 5.To check set bits
 6.To clear from LSB to nth bit positon
 7.Count the number of set bits
 8.EXIT
 Enter your choice
 2    
 Enter the data
 10
 Enter the bit
 1
 Data before clearing a bit
 00000000000000000000000000001010
 Data after clearing 1 bit
 00000000000000000000000000001000

 *********MENU FOR BIT MANIPULATIONS*************
 1.set a bit
 2.clear a bit
 3.Toggle a bit
 4.Check bit status at a position
 5.To check set bits
 6.To clear from LSB to nth bit positon
 7.Count the number of set bits
8.EXIT
Enter your choice
3
Enter the data
8
Enter the bit
2
Data before toggling a bit
00000000000000000000000000001000
Data after toggling 2 bit
00000000000000000000000000001100

*********MENU FOR BIT MANIPULATIONS*************
1.set a bit
2.clear a bit
3.Toggle a bit
4.Check bit status at a position
5.To check set bits
6.To clear from LSB to nth bit positon
7.Count the number of set bits
8.EXIT
Enter your choice
4
enter the position of bit which you want to check the status:
7
entered data is : 8
00000000000000000000000000001000
bit at position 7 is :0

*********MENU FOR BIT MANIPULATIONS*************
1.set a bit
2.clear a bit
3.Toggle a bit
4.Check bit status at a position
5.To check set bits
6.To clear from LSB to nth bit positon
7.Count the number of set bits
8.EXIT
Enter your choice
5
entered data is :8
00000000000000000000000000001000
lowest set bit is :3

*********MENU FOR BIT MANIPULATIONS*************
1.set a bit
2.clear a bit
3.Toggle a bit
4.Check bit status at a position
5.To check set bits
6.To clear from LSB to nth bit positon
7.Count the number of set bits
8.EXIT
Enter your choice
6
please enter the position till where you want to clear the bit
3
entered data before clearing:8
00000000000000000000000000001000
the data after clearing upto bit position 3 is :0
00000000000000000000000000000000

*********MENU FOR BIT MANIPULATIONS*************
1.set a bit
2.clear a bit
3.Toggle a bit
4.Check bit status at a position
5.To check set bits
6.To clear from LSB to nth bit positon
7.Count the number of set bits
8.EXIT
Enter your choice
7
the number of set bits in given data 8 = 1

*********MENU FOR BIT MANIPULATIONS*************
1.set a bit
2.clear a bit
3.Toggle a bit
4.Check bit status at a position
5.To check set bits
6.To clear from LSB to nth bit positon
7.Count the number of set bits
8.EXIT
Enter your choice
8

***********************************************************************************************************************************/
