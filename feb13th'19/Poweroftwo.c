/******************************************************************************************
  NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
  ____________________________________________________________________________________________
  JAVED ALAM           96163388563             javedalam145@gmail.com          10336
  -------------------------------------------------------------------------------------------- 
  Purpose:-
 ----------------
 This program is the implementation of counting number of digits in the given data.
**************************************************************************************************************************/


#include<stdio.h>
int main(){
	int data;
	int count = 0;
	printf("enter the data\n");
	scanf("%d",&data);
	for(int i=0;i<=31;i++){
		if((data>>i)&1){
			count++;
		}
	}
	if(count == 1){
		printf("%d is the power of 2\n",data);
	}
	else{
		printf("%d is not the power of 2\n",data);
	}
	return 0;
}

/*******************************OUTPUT*********************************
enter the data
7
7 is not the power of 2
----------------------------------------------
enter the data
4
4 is the power of 2
***********************************************************************/
