/******************************************************************************************
  NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
  ____________________________________________________________________________________________
  JAVED ALAM           96163388563             javedalam145@gmail.com          10336
  -------------------------------------------------------------------------------------------- 
  Purpose:-
 ----------------
 This program is the implementation checking given number is even or odd.
**************************************************************************************************************************/

#include<stdio.h>

int main(){
	//declaring the variables
	int data;
	printf("Enter the data\n");
	scanf("%d",&data);
	//condition for even or odd
	if(data&1){
		printf("odd number\n");
	}else{
		printf("Even number\n");
	}
	return 0;
}
/***************************OUTPUT FOR THE ABOVE PROGRAM************************************
 Enter the data
5
odd number
javed@javed-Aspire-4738: ./a.out
Enter the data
4
Even number
javed@javed-Aspire-4738: ./a.out
Enter the data
11
odd number
******************************************************************************************/
