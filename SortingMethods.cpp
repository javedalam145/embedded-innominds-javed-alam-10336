/*******************************************************************************************
  NAME                  CONTACT NO.             EMAIL-ID                        EMPLOYEE ID
--------------------------------------------------------------------------------------------
JAVED ALAM              96163388563             javedalam145@gmail.com          10336
********************************************************************************************
This program is for implementation of sorting techniques which includes selection sort, 
insertion sort, bubble sort and merge sort. 

********************************************************************************************/
#include<iostream>
#include<cstdio>
#include<cstdlib>

using namespace std; 
//creating class for implementing sorting techniques
class SortingMethods {
	//declaring the member variable
	int *array;
	int size; 

	public:
	SortingMethods(int size) { 		//creating default constructor
		this->size = size;
		array = new int[size];
		cout<<"enter the "<<size<<" array elements"<<endl;
		for(int i=0;i<size;i++)		//taking the input array from the user
			cin>>array[i];
	}
	//default deconstructor
	~SortingMethods() {
		delete array;
	}
	//method for implementation of insertion sort
	void doInsertionSort( ) {
		int temp,i,j,m;
		for (i = 1 ; i <= size-1 ; i++ ) {	
			for (j = 0 ; j < i ; j++ ) {
				if ( array[j] > array[i] ) {
					temp = array[j];
					array[j] = array[i];
					for ( m = i ; m > j ; m-- )
						array[m] = array[m-1];
					array[m+1] = temp;
				}
			}
		}
	}
	//method for the implementation of selection sort
	void doSelectionSort() 	{
		// One by one move boundary of unsorted subarray 
		for (int i = 0; i < size-1; i++) { 
			// Find the minimum element in unsorted array 
			for (int j = i+1; j < size; j++) 
				if (array[i] > array[j]) 
					// Swap the found minimum element with the first element 
					doSwap(array[i], array[j]); 
		} 
	} 
	//method for swapping the array
	void doSwap(int &a, int &b) 
	{ 
		int temp = a; 
		a = b;
		b = temp;	
	}
	//method for the implementation of bubble sort 
	void doBubbleSort() { 
		int i, j; 
		for (i = 0; i < size-1; i++)       
			// Last i elements are already in place    
			for (j = 0; j <size-i-1;j++)  
				if (array[j] > array[j+1]) 
					// Swap the found minimum element with the first element 

					doSwap(array[j],array[j+1]); 
	} 

	// Function to print an array
	void printArray() 
	{ 
		int i; 
		for (i=0; i < size; i++) 
			cout<<array[i]<<" "; 
		cout<<endl; 
	} 
};
// Driver program to test above functions 
int main() 
{ 
	SortingMethods *sort = new SortingMethods(10);
	int choice;
	while(1){
		cout<<"what sorting method you want to apply??"<<endl;
		cout<<"1.Selection sort"<<endl<<"2.Insertion sort"<<endl<<"3.Bubble sort"<<endl<<"4.EXIT"<<endl;
		cin>>choice;
		switch(choice){
			case 1:	sort->doSelectionSort();
				cout<<"Sorted array: "<<"\n"<<endl; 
				sort->printArray();
				break;
			case 2: sort->doInsertionSort(); 
				cout<<"Sorted array: "<<"\n"<<endl; 
				sort->printArray();
				break;
			case 3: sort->doBubbleSort(); 
				cout<<"Sorted array: "<<"\n"<<endl; 
				sort->printArray();
				break;
			case 4:
				exit(0);
			default:cout<<"please give proper input";
		}
	}

	return 0; 
} 
/*************************output of the program**************************************************
enter the 10 array elements
5
4
6
3
2
1
9
8
8
7
what sorting method you want to apply??
1.Selection sort
2.Insertion sort
3.Bubble sort
4.EXIT
3
Sorted array: 

1 2 3 4 5 6 7 8 8 9 
what sorting method you want to apply??
1.Selection sort
2.Insertion sort
3.Bubble sort
4.EXIT
4
*************************************************************************************************/
