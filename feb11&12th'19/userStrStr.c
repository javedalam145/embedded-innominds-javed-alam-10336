/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM           96163388563             javedalam145@gmail.com          10336
-------------------------------------------------------------------------------------------- 
Purpose
-----------
 This program is the implementation of userdefined strstr function,where we can find the given
 substring in the main string
 **********************************************************************************************************/



#include <stdio.h>
#include <string.h>
//declaring the userdefined strstr function
char * userstrstr(char*,char*);
int main(void) {
	//declaring the string1 and string2 character arrays
	char str1[20],str2[10],*ptr;
	printf("Enter the 1st string :");
	gets(str1);
	printf("Enter the 2st string :");
	gets(str2);
	if(userstrstr(str1,str2))
		printf("Sub string found\n");
	else
		printf("String Not found\n");

	return 0;
}
//function defination for userdefined strstr
char * userstrstr(char *str1,char*str2) {

	char i,j;
	int len,flag=0;
	//finding the length of string1
	len = strlen(str1);
	for(i=0 ; i<len ; i++) {
		if(str1[i] == str2[0]) {
			for(j=1;str2[j];j++) {
				if(str1[i+j] == str2[j]) {
					flag=1;
					continue;
				}
				else {
				       flag=0;	
					break;
				}
			}
		}
	}
	if(flag == 1)
		return i;
	else
		return 0;
}
/********************************************OUTPUT OF THE PROGRAM**********************************************
Enter the 1st string :hema latha
Enter the 2st string :latha
Sub string found
*****************************************************************************************************************/ 
