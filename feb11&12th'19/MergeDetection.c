/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM           96163388563             javedalam145@gmail.com          10336
-------------------------------------------------------------------------------------------- 
Purpose
-----------
 This program gives the address as output when two single lists have any  common address at any position of 2nd linkedlist
****************************************************************************************************************************/
#include<stdio.h>
#include<stdlib.h>

int count=0;
int c=0;
//declaration of structure
struct node{
	//declaring variables
	int data;
	struct node *next;
};

struct node *address=NULL;
struct node *address1=NULL;

//function to mergedetection
void mergeDetection(struct node *temp,struct node *head1){
	int i;
	struct node *temp1=temp;
	if(temp==NULL){
		printf("not found\n");
	}else{
		for(head1;head1->next!=NULL;head1=head1->next) {
			while(temp1!=NULL && temp1!=head1) {
				temp1 = temp1->next;

			}
			if(temp1!=NULL) {
				address1=temp1;
				break;
			}

			temp1=temp;
		}
	}
	if(address!=address1) {
		printf("Merge not found\n");

	}
	else {
		printf("merge found in both linkedlist at address  %p \n",address1);
	}
}

//adds data to the list
void addatEnd(struct node **head){
	struct node *temp;
	struct node *newnode = (struct node*)malloc(sizeof(struct node*));
	printf("Enter the data into the list\n");
	scanf("%d",&newnode->data);
	newnode->next = NULL;
	if(*head==NULL){
		count++;
		*head=newnode;
	}else{
		for(temp=*head;temp->next!=NULL;temp=temp->next);

		if(count == 4) {
			address = temp;

		}
		temp->next = newnode;
		count++;
	}

}

//displaying the data
int displayingData(struct node *head){
	struct node *traverse = head;
	int count=0;
	printf("--------------------------------------\n");
	while(traverse){
		printf("%d  ",traverse->data);
		traverse = traverse->next;
		count++;
	}
	printf("\n------------------------------------------\n");
	return count;
}

//method to run above functions
int main(){
	//declaring the variables
	int count1=0,count2=0;
	struct node *head = NULL;
	struct node *head1 = NULL;
	struct node *temp = NULL;
	
	printf("enter the elemets of 1st singlelist \n");
	for(int i=0;i<8;i++){
		addatEnd(&head);
	}

	printf("displaying the elements of 1st linked list \n");
	count1=displayingData(head);

	printf("enter the elemets of 2nd singlelist \n");
	for(int i=0;i<4;i++) {

		addatEnd(&head1);
	}

	printf("display the elements of 2nd linked list  before merge with 1st linked list \n");
	count2=displayingData(head1);
	if(count-count2>4) {
	for(temp=head1;temp->next!=NULL;temp=temp->next);
	temp->next = address;


	printf("display the elements of 2nd linked list  after merge with 1st linked list \n");
	displayingData(head1);

	mergeDetection(head1,head);
	} else {
		printf("merge was not possible since the 1st single list length %d is not greater than 4\n",count1);
	}
	return 0;

}
/****************OUTPUT********************************
enter the elemets of 1st singlelist 
Enter the data into the list
2
Enter the data into the list
3
Enter the data into the list
4
Enter the data into the list
5
Enter the data into the list
6
Enter the data into the list
7
Enter the data into the list
8
Enter the data into the list
9
displaying the elements of 1st linked list 
--------------------------------------
2  3  4  5  6  7  8  9  
------------------------------------------
enter the elemets of 2nd singlelist 
Enter the data into the list
20
Enter the data into the list
30
Enter the data into the list
40
Enter the data into the list
50
display the elements of 2nd linked list  before merge with 1st linked list 
--------------------------------------
20  30  40  50  
------------------------------------------
display the elements of 2nd linked list  after merge with 1st linked list 
--------------------------------------
20  30  40  50  5  6  7  8  9  
------------------------------------------
merge found in both linkedlist at address  0x55ffe5f39ae0  

**********************************************************/
