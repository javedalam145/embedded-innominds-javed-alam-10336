/*******************************************************************************************
NAME                  PHONE NUMBER           EMAIL ID                           EMPLOYEE ID
--------------------------------------------------------------------------------------------

JAVED ALAM            96163388563           javedalam145@gmail.com              10336


------------------------------------------------------------------------------------------
       This program is rotating the given matrix by 90 degrees using two dimensional
	array in C.

-------------------------------------------------------------------------------------------*/


#include<stdio.h>
#include<stdlib.h>

int main() {
	//declaration of the variables
	int i,j,rows,columns;
	//taking user defined rows and columns
	printf("enter the rows and columns\n");
	scanf("%d%d",&rows,&columns);
	//declaration of arrays and resultant matrix
	int array[rows][columns];
	int resultantMatrix[columns][rows];
	//taking the matrix
	printf("enter the matrix element\n");
	for(i =0;i<rows;i++) {
		for(j=0;j<columns;j++) {
			scanf("%d",&array[i][j]);
		}
	}
	//displaying the matrix before rotation
	printf("the array before rotating\n");
	for(i=0;i<rows;i++){
		printf("\n");
		for(j=0;j<columns;j++){
			printf("%d",array[i][j]);
		}
	}
	//operation on matrix to rotate it through 90 degrees
	for(i=0,j=rows-1;i<rows && j>=0;i++,j--) {
		for(int k =0;k<columns;k++){
			resultantMatrix[k][j] = array[i][k];
		}
	}
	//displaying after the rotation
	printf("\n matrix after rotation is :\n");
	for(i=0;i<columns;i++) {
		printf("\n");
		for(j=0;j<rows;j++) {
			printf("%d  ",resultantMatrix[i][j]);
		}
		printf("\n");
	}
	return 0;
}
/*************output of the program***********
  enter the rows and columns
  4 5
  enter the matrix element
  1 2 3 4 5 6 7 8 9 8 7 6 5 4 3 2 1 1 2 3 
  the array before rotating

  12345
  67898
  76543
  21123
  matrix after rotation is :

  2  7  6  1  

  1  6  7  2  

  1  5  8  3  

  2  4  9  4  

  3  3  8  5  
 *************************************************/
