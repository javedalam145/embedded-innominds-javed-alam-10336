/*******************************************************************************************
NAME                  PHONE NUMBER           EMAIL ID                           EMPLOYEE ID
--------------------------------------------------------------------------------------------

JAVED ALAM            96163388563           javedalam145@gmail.com              10336


------------------------------------------------------------------------------------------
       This program is for multiplication of two matrix using two dimensional
       array in C.

-------------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<stdlib.h>

int main() {
	//declaration of matrices and other variables
	int mat1[10][10],mat2[10][10],resultantMatrix[10][10],row1,col1,row2,col2,i,j,k;
	//taking user input for row and column for the two matrices 
	printf("Enter the rows and column for first matrix:\n");
	scanf("%d %d",&row1,&col1);
	printf("Enter the rows and column for second matrix:\n");
	scanf("%d %d",&row2,&col2);
	//checking the multiplication condition for the matrices
	while(col1 != row2) {
		printf("Error! column of first matrix not equal to row of 2nd matrix\n");
		printf("enter the row and column for the first matrix:\n");
		scanf("%d%d",&row1,&col1);
		printf("enter the row and column for the 2nd matrix:\n");
		scanf("%d%d",&row2,&col2);
	}
	//taking the first matrix from user
	printf("\nEnter elements of matrix:\n");
	for(i = 0;i<row1;i++) {
		for(j=0;j<col1;j++) {
			printf("Enter elements mat1%d%d:",i+1,j+1);
			scanf("%d",&mat1[i][j]);
		}
	}
	//taking the second matrix from user
	printf("\nEnter elements of matrix 2:\n");
	for(i =0;i<row2;i++) { 
		for(j=0;j<col2;j++) {
			printf("enter elements mat2%d%d:",i+1,j+1);
			scanf("%d",&mat2[i][j]);
		}
	}
	//multiplying the two matrices
	for(i=0;i< row1;i++) {
		for(j=0;j < col2;j++) {
			//initialising the elements of resultMatrix to zero
			resultantMatrix[i][j] = 0;
			for(k=0;k< row2;k++) {
				resultantMatrix[i][j] += mat1[i][k] * mat2[k][j];
			}
		}
	}

	//displaying the resultant matrix
	printf("\n the resultant matrix is :\n");
	for(i=0;i<row1;i++)
		for(j=0;j<col2;j++) {
			printf("%d  ",resultantMatrix[i][j]);
			if(j == col2 - 1)
				printf("\n\n");
		}
	return 0;
}

/******************output of the program********************
Enter the rows and column for first matrix:
3
2
Enter the rows and column for second matrix:
3
3
Error! column of first matrix not equal to row of 2nd matrix
enter the row and column for the first matrix:
3   
2
enter the row and column for the 2nd matrix:
2
3

Enter elements of matrix:
Enter elements mat111:1
Enter elements mat112:2
Enter elements mat121:3
Enter elements mat122:4
Enter elements mat131:5
Enter elements mat132:6

Enter elements of matrix 2:
enter elements mat211:9
enter elements mat212:8
enter elements mat213:7
enter elements mat221:6
enter elements mat222:5
enter elements mat223:4

 the resultant matrix is :
21  18  15  

51  44  37  

81  70  59  
******************************************************************/
