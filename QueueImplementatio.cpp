/*******************************************************************************************
  NAME                  CONTACT NO.             EMAIL-ID                        EMPLOYEE ID
--------------------------------------------------------------------------------------------
JAVED ALAM              96163388563             javedalam145@gmail.com          10336
********************************************************************************************
this program is the implementation of queues in linked list, where the datas are being added 
from the rear end and popped from the front end.
******************************************************************************************/


#include<iostream>
#include<stdlib.h>

using namespace std;
//declaring the node structure for the queue
struct Node{
	int data;
	Node *next;
};
//class creation of MyQueue type
class MyQueue{
	public:
		//node attributes declaration
		Node *front,*rear;
		//default MyQueue constructor
		MyQueue(){
			front = rear = NULL;
		}
		//member function declaration
		void enQueue(int n);
		void deQueue();
		void traverse();
		~MyQueue(); 		//default deconstructor 
};
//methot to push the data into the queue
void MyQueue::enQueue(int data){
	Node *temp = new Node;
	if(temp == NULL){
		cout<<"Overflow"<<endl;
		return;
	}
	temp->data = data;
	temp->next = NULL;

	//for first node
	if(front == NULL){
		front = rear = temp;
	}
	else{
		rear->next = temp;
		rear = temp;
	}
	cout<<data<<" has been inserted successfully."<<endl;
}
//method to pop the data from the queue from front end

void MyQueue::traverse() {
	if(front == NULL) {
		cout<<"Underflow."<<endl;
		return;
	}
	Node *temp = front;
	//will check until NULL is not found
	while(temp) {
		cout<<temp->data<<" ";
		temp = temp->next;
	}
	cout<<endl;
}

void MyQueue :: deQueue()
{
	if (front == NULL){
		cout<<"underflow"<<endl;
		return;
	}

	cout<<front->data<<" is being deleted "<<endl;
	if(front == rear)//if only one node is there
		front = rear = NULL;
	else
		front = front->next;
}
//default deconstructor method
MyQueue ::~MyQueue()
{
	while(front != NULL) {
		Node *temp = front;
		front = front->next;
		delete temp;
	}
	rear = NULL;
}

//main method to test the above functionality
int main() {
	//declaring the variables
	int choice;
	int data;
	MyQueue queue;
	while(1){
		cout<<"************MENU**************"<<endl;
		cout<<"1.push data into the queue"<<endl<<"2.pop data from the queue"<<endl<<"3.displaying the data"<<endl<<"4.EXIT"<<endl;
		cin>>choice;
		switch(choice){
			case 1:cout<<"enter the data"<<endl;
			       cin>>data;
			       queue.enQueue(data);
			       break;
			case 2: cout<<"enter teh data to dequeue"<<endl;
				cin>>data;
				queue.deQueue();
				break;
			case 3:queue.traverse();
			       break;
			case 4:
			       exit(0);
			default:cout<<"please give proper input";
		}
	}
	return 0;
}
/********************************output of the program*************************************
************MENU**************
1.push data into the queue
2.pop data from the queue
3.displaying the data
4.EXIT
1
enter the data
12
12 has been inserted successfully.
************MENU**************
1.push data into the queue
2.pop data from the queue
3.displaying the data
4.EXIT
1
enter the data
4
4 has been inserted successfully.
************MENU**************
1.push data into the queue
2.pop data from the queue
3.displaying the data
4.EXIT
3
12 4 
************MENU**************
1.push data into the queue
2.pop data from the queue
3.displaying the data
4.EXIT
2
enter teh data to dequeue
4
12 is being deleted 
************MENU**************
1.push data into the queue
2.pop data from the queue
3.displaying the data
4.EXIT
3
4 
************MENU**************
1.push data into the queue
2.pop data from the queue
3.displaying the data
4.EXIT
4 
*************************************************************************************/
