/******************************************************************************************
 NAME               MOBILE NUMBER            EMAIL ID                        EMPLOYEE ID 
____________________________________________________________________________________________
JAVED ALAM          96163388563              javedalam145@gmail.com          10336
---------------------------------------------------------------------------------------------
This program is implementation of user defined shell(grep is not implemented)

*********************************************************************************************/
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
char * arguments[100];

//reading userfrom the stdin
void readComandFromUser() {
	char *string;
	int i=0;
	string=(char *)malloc(20);
	read(0,string,100);
	string[strlen(string)-1]='\0';
	// Returns first token
	char* token = strtok(string, " |");

	while (token != NULL) {
		arguments[i]=token;
		printf("%s\n",arguments[i]);
		i++;
		token = strtok(NULL, " |");
	}
	arguments[i]=token;
}
//main function to test above functionality
int main() {
	int ret = -1,child_exit_status;
	char *userName;
	char cwd[1024];			
	//geting user name
	getlogin_r(userName,10);
	userName[strlen(userName)]='@';
	char name[] = "myShell$ ";
	strcat(userName,name);
	while(1) {
		//writing our own username@shellname to the STDOUT
		write(1,userName, strlen(userName));
		readComandFromUser();
		//if entered string equals to PWD
		if(!(strcmp(arguments[0],"pwd"))) {

			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
			continue;
		}
		//if entered string equals to Exit
		else if(!(strcmp(arguments[0],"exit"))) {

			exit(0);
		}
		//if entered string equals tp cd
		else if(!(strcmp(arguments[0],"cd"))) {

			chdir(arguments[1]);
			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
			continue;
		}


		//if entered string is any linux command 
		ret = fork();
		if(ret == 0) {

			execvp(arguments[0], arguments);
		}
		else
		{
			wait(&child_exit_status);
		}
	}
	return 0;
}
/***********************************output of the above program***************************************
javed@myShell$ ls
ls
 8-2-2019	    data_struct				  lect1004.pdf
 a.out		    embedded-innominds-hemalatha-10374	  linux
 Bits.pdf	    embedded-innominds-javed-alam-10336   myshell.c
 C		    embedded-innominds-naveena-10357	  programs
 CircuilarQueue.c  "feb_8th'19"				  programs.zip
 cpp		    files.zip				  rec19.pdf
javed@myShell$ pwd
pwd
/home/javed/trainingVizag
javed@myShell$ exit
exit

************************************************************************************/

